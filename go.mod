module bitbucket.org/snapmartinc/eventbus-client

go 1.12

require (
	bitbucket.org/snapmartinc/logger v0.0.0-20190722102907-70e1fed01587
	bitbucket.org/snapmartinc/newrelic-context v0.0.0-20200519100555-b5fc0fc1047c
	bitbucket.org/snapmartinc/trace v0.0.0-20190925102910-0918afc8a51f
	bitbucket.org/snapmartinc/user-service-client v0.0.0-20190916112339-fefd7d2a4d59
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-redis/redis/v8 v8.0.0-beta.1
	github.com/jinzhu/gorm v1.9.12
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/newrelic/go-agent v2.14.1+incompatible
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	golang.org/x/lint v0.0.0-20190313153728-d0100b6bd8b3 // indirect
	golang.org/x/tools v0.0.0-20190524140312-2c0ae7006135 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
	gopkg.in/redis.v5 v5.2.9 // indirect
	honnef.co/go/tools v0.0.0-20190523083050-ea95bdfd59fc // indirect
)
